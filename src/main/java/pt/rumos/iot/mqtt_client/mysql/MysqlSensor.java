package pt.rumos.iot.mqtt_client.mysql;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="sensor")
public class MysqlSensor {
    
    @Id
    private Long id;
    private Date timestamp;
    private String temperature;

}