package pt.rumos.iot.mqtt_client.mysql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service

public class MysqlService {
    
    @Autowired
    private MysqlRepository repo;

    public void save(MysqlSensor t) {
        repo.save(t);
    }

    @ModelAttribute("temperature")
    public List<MysqlSensor> getAll() {
        return (List<MysqlSensor>) repo.findAll();
    }
}
