package pt.rumos.iot.mqtt_client.redis;

import javax.persistence.Id;

import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@RedisHash("sensor")
@Data
public class Temperature {
    
    @Id
    private Long id;
    private String temperature;

}