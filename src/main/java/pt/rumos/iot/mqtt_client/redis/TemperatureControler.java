package pt.rumos.iot.mqtt_client.redis;
import java.util.Optional;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TemperatureControler {
    
    @Autowired
    private TemperatureService service;

    @GetMapping("/realtime")
    public String getLatest(Model m) {
        Optional<Temperature> data = service.findSensor(1L);
        if (data.isPresent()) {
            return new Gson().toJson(data.get());
        }
        else {
            return new Gson().toJson("");
        }
        
    }

}
