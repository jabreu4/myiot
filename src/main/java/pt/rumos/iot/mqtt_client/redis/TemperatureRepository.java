package pt.rumos.iot.mqtt_client.redis;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TemperatureRepository extends CrudRepository<Temperature, Long> {
    
}
